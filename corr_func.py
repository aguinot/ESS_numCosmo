#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 11:19:49 2017

@author: aguinot
"""

try:
    import gi
    gi.require_version('NumCosmo', '1.0')
    gi.require_version('NumCosmoMath', '1.0')
except:
    pass

import numpy
import scipy.integrate as sci
from math import *
import matplotlib.pyplot as plt
from gi.repository import GObject
from gi.repository import NumCosmo as Nc
from gi.repository import NumCosmoMath as Ncm




#
#  Initializing the library objects, this must be called before 
#  any other library function.
#
Ncm.cfg_init ()


#
#  New homogeneous and isotropic cosmological model NcHICosmoDEXcdm 
#
cosmo = Nc.HICosmo.new_from_name (Nc.HICosmo, "NcHICosmoDEXcdm")


#
#  New cosmological distance objects optimizied to perform calculations
#  up to redshift 10.0.
#
dist = Nc.Distance.new (10.0)


#
# New transfer function 'NcTransferFuncEH' using the Einsenstein, Hu
# fitting formula.
#
tf = Nc.TransferFunc.new_from_name ("NcTransferFuncEH")
tf.prepare(cosmo)

gf = Nc.GrowthFunc.new()
gf.prepare (cosmo)

#
# New linear matter power spectrum object based of the EH transfer function.
# 
psml = Nc.PowspecMLTransfer.new (tf)
psml.require_kmin (1.0e-3)
psml.require_kmax (2.0e3)


#
# Apply a tophat filter to the psml object, set best output interval.
#
psf = Ncm.PowspecFilter.new (psml, Ncm.PowspecFilterType.TOPHAT)
psf.set_best_lnr0 ()

#
# New multiplicity function 'NcMultiplicityFuncTinkerMean'
#
mulf = Nc.MultiplicityFunc.new_from_name ("NcMultiplicityFuncTinkerMean")

#
# New mass function object using the objects defined above.
#
mf = Nc.HaloMassFunction.new (dist, psf, mulf)

#
#  Setting values for the cosmological model, those not set stay in the
#  default values. Remember to use the _orig_ version to set the original
#  parameters when a reparametrization is used.
#
cosmo.props.H0      = 70.0
cosmo.props.Omegab  = 0.05
cosmo.props.Omegac  = 0.25
cosmo.props.Omegax  = 0.70
cosmo.props.Tgamma0 = 2.72
cosmo.props.w       = -1.0


#
#  Printing the parameters used.
#
print "# Model parameters: ", 
cosmo.params_log_all ()


#
#   Initialize some parameters
#

l_min=1
l_max=2000
nl=l_max-l_min+1
ell= list(numpy.linspace(l_min,l_max,nl))
v_ell = Ncm.Vector.new_array(ell)
v_ps = Ncm.Vector.new(nl)

z_min=0.
z_max=5.
z_0=0.96


#
#   Create galaxy correlation object
#

gal_corr=Nc.XcorLimber.new_from_name("NcXcorLimberGal")


#
#   define dn/dz from https://arxiv.org/pdf/1306.0534.pdf TABLE I
#

z=numpy.linspace(z_min,z_max,500)
dn_dz=z*z*numpy.e**(-(z/z_0)**(3./2.))


#
#   Set Gal_kernel parameters

gal_corr.set_dNdz(z,dn_dz)

gal_corr.props.zmax = z_max
gal_corr.props.zmin = z_min


#
#   Create lensing correlation object
#

lens_corr=Nc.XcorLimber.new_from_name("NcXcorLimberLensing")


#
#   Set Lens_kernel paraeters
#

lens_corr.props.Nl = v_ell
lens_corr.props.dist = dist


#
#   galxgal
#

def func(xcl1, xcl2, cosmo,dist,tf,gf,z,l):
    
    xi_z=Nc.Distance.comoving(dist,cosmo,z)
#    print(xi_z)
    k=(l+0.5)/(xi_z*Ncm.C.hubble_radius_hm1_Mpc())
#    print(k)
    tf_tmp = tf.eval(cosmo,k)
#    print(tf_tmp)
    gf_tmp = gf.eval(cosmo,z)
#    print(gf_tmp)
    power_spec = k**0.96 * tf_tmp**2. * gf_tmp**2.
#    print(power_spec)
#    kernel=gal_corr.eval_kernel(cosmo,z,l)*gal_corr.eval_kernel(cosmo,z,l)
    kernel=xcl1.eval_kernel(cosmo,z,l)*xcl2.eval_kernel(cosmo,z,l)
#    kernel=lens_corr.eval_kernel(cosmo,z,l)*lens_corr.eval_kernel(cosmo,z,l)
#    print(kernel)
#    E_z = Nc.HICosmo.E(cosmo,z)
    H_z = cosmo.H(z)
#    print(E_z)
    
    return Ncm.C.c()*H_z * kernel * power_spec /(xi_z**2.)

l=[]
psgl=[]

for i in range(l_min,l_max+1,1):
    l.append(i)
    psgl.append(sci.quad(lambda z: func(gal_corr, lens_corr, cosmo,dist,tf,gf,z,i), z_min, z_max)[0])
    
psgl=numpy.asarray(psgl)

l=[]
psgg=[]

for i in range(l_min,l_max+1,1):
    l.append(i)
    psgg.append(sci.quad(lambda z: func(gal_corr, gal_corr, cosmo,dist,tf,gf,z,i), z_min, z_max)[0])

psgg=numpy.asarray(psgg)

l=[]
psll=[]

for i in range(l_min,l_max+1,1):
    l.append(i)
    psll.append(sci.quad(lambda z: func(lens_corr, lens_corr, cosmo,dist,tf,gf,z,i), z_min, z_max)[0])

l=numpy.asarray(l)
psll=numpy.asarray(psll)


plt.figure()
plt.title('Power spectrum')
plt.plot(l,psgg*l*(l+1)/2.,label='<gal x gal>')
plt.plot(l,psll*l*(l+1)/2.,label='<lens x lens>')
plt.plot(l,psgl*l*(l+1)/2.,label='<lens x gal>')

plt.yscale('log')
plt.xscale('log')
plt.xlabel('l')
plt.ylabel('C(l)*l(l+1)/2')

plt.legend()

plt.show()

#galgal_corr=Nc.Xcor.new(dist, tf, gf, z_min, z_max)



#galgal_corr.limber_cross_cl(gal_corr,gal_corr,cosmo,v_ell,v_ps,0)


